# 3.0.0

- **[BREAKING CHANGE]** Updated default import to no longer import required CSS file.
  If you wish to continue to use this functionality, import `react-linear-layout/with-css`
  instead of `react-linear-layout` _(see README for more details)_.


# 2.0.2

- Updated dev dependencies.


# 2.0.1

- **[BUG FIX]** Fixed SSR compatibility.


# 2.0.0

- **[BREAKING CHANGE]** Updated `main` and `module` in `package.json` such that `main`
  now points to CommonJS version of library, and `module` points to ESM version.
- Upgraded to latest versions of several dev dependencies.


# 1.0.1

- README typos fixed.


# 1.0.0

- Initial release _(separated from @bsara/react-layouts package)_.
