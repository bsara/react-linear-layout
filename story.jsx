/**
 * ISC License (ISC)
 *
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
import React         from 'react';
import { storiesOf } from '@storybook/react';

import LinearLayout from './with-css';

import './story.css';



const contrastStyles = {
  border: '1px dotted black'
};

const itemContrastStyles = {
  backgroundColor: '#eee',
  border:          '1px solid gray'
};



storiesOf('LinearLayout', module)
  .add('Default', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('README Basic Usage Example', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout className="_ll-readme-layout" direction="horizontal">
        <a>Anchor 0</a>
        <a>Anchor 1</a>
        <a>Anchor 2</a>
        <LinearLayout className="_ll-readme-sub-layout" direction="vertical">
          <a>Sub Anchor 0</a>
          <a>Sub Anchor 1</a>
          <a>Sub Anchor 2</a>
        </LinearLayout>
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Item Alignment (Start)', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout className="_ll-align-items-start" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Item Alignment (End)', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout className="_ll-align-items-end" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Item Alignment (Center)', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout className="_ll-align-items-center" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Horizontal', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout direction="horizontal" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Horizontal (Item Gap)', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout direction="horizontal" className="_ll-item-gap" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Horizontal (Stretch Items)', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout className="_ll-stretch-items" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Vertical', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout direction="vertical" className="_ll-vert" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Vertical (Tall)', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout direction="vertical" className="_ll-tall" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Vertical (Item Gap)', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout direction="vertical" className="_ll-vert _ll-item-gap" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ))
  .add('Vertical (Stretch Items)', () => (
    <ResponsiveComponentNoticeWrapper>
      <LinearLayout className="_ll-vert _ll-stretch-items" direction="vert" style={contrastStyles}>
        {_generateItems()}
      </LinearLayout>
    </ResponsiveComponentNoticeWrapper>
  ));




function ResponsiveComponentNoticeWrapper(props) {
  return (
    <div>
      <span style={{ fontFamily: 'sans', fontSize: '12px', fontStyle: 'italic' }}>
        *If you need a linear layout that is responsive, check out <a href="https://npmjs.com/package/react-responsive-linear-layout"><code style={{ fontFamily: 'monospace', fontStyle: 'normal' }}>react-responsive-linear-layout</code></a>.
      </span>
      <br />
      <br />
      {props.children}
    </div>
  );
}


function _generateItems() {
  const items = [];

  for (let i = 0; i < 15; i++) {
    items.push(<div style={itemContrastStyles} key={i}>Item {i}</div>);
  }

  return items;
}
