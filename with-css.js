// eslint-disable-next-line spaced-comment
/*!
 * react-linear-layout v3.0.1
 *
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/react-linear-layout/blob/master/LICENSE)
 */
import LinearLayout from '.';
import './react-linear-layout.css';



export default LinearLayout;
