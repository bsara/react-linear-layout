/*!
 * react-linear-layout 3.0.1
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-linear-layout/blob/master/LICENSE)
 */
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var PropTypes = _interopDefault(require('prop-types'));
var pickHtmlAttributeProps = require('pick-html-attribute-props/node');

function _LinearLayout(props, ref) {
  const isDirectionVertical = (props.direction === 'v' || props.direction === 'vert' || props.direction === 'vertical');


  const classNames = [];

  if (props.className) {
    classNames.push(props.className);
  }

  classNames.push('rll__layout');
  classNames.push(isDirectionVertical ? 'rll__vert' : 'rll__horiz');

  if (!props.omitItemGap) {
    classNames.push(isDirectionVertical ? 'rll__vert-with-item-gap' : 'rll__horiz-with-item-gap');
  }

  if (props.inline) {
    classNames.push('rll__inline');
  }

  if (props.wrap) {
    classNames.push('rll__wrapped');
  }


  return React.createElement(
    "div",

    Object.assign(
      {},
      pickHtmlAttributeProps.pickGlobalHtmlAttrProps(props),
      {
        ref,
        className: classNames.join(" ")
      }
    ),

    props.children
  );
}



const LinearLayout = React.forwardRef(_LinearLayout);

LinearLayout.propTypes = {
  direction:   PropTypes.oneOf(['h', 'horiz', 'horizontal', 'v', 'vert', 'vertical']),
  inline:      PropTypes.bool,
  wrap:        PropTypes.bool,
  omitItemGap: PropTypes.bool
};

module.exports = LinearLayout;
