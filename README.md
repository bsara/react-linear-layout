# react-linear-layout [![NPM Package](https://img.shields.io/npm/v/react-linear-layout.svg?style=flat-square)][npm]

[![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square)][license]

> A layout component that arranges children in a single row or column using CSS flexbox.

At it's core, this component is basically a [CSS flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox).
As such, all CSS flexbox properties are valid when styling a `LinearLayout`. The component
consists of some sensible CSS defaults as well as a few **[CSS custom properties](#css-custom-properties)**
_(see the documented CSS custom properties below for more details)_ that make styling item
gaps _(I.E. margins)_ and sizing of elements quite a bit easier.


[Storybook](https://bsara.gitlab.io/react-linear-layout)

[Changelog](https://gitlab.com/bsara/react-linear-layout/blob/master/CHANGELOG.md)


# Install

```bash
$ npm install --save react-linear-layout
```


# Basic Usage

[Storybook Examples](https://bsara.gitlab.io/react-linear-layout)

**MyLinearLayoutComponent.jsx**

```jsx
import React from 'react';

import LinearLayout from 'react-linear-layout';
import 'react-linear-layout/react-linear-layout.css';

import './MyLinearLayoutComponent.css';


export default function MyLinearLayoutComponent(props) {
  return (
    <LinearLayout {...props} className="layout" direction="horizontal">
      <a>Anchor 0</a>
      <a>Anchor 1</a>
      <a>Anchor 2</a>
      <LinearLayout className="sub-layout" direction="vertical">
        <a>Sub Anchor 0</a>
        <a>Sub Anchor 1</a>
        <a>Sub Anchor 2</a>
      </LinearLayout>
    </LinearLayout>
  );
}
```


**MyLinearLayoutComponent.css**

```css
.layout {
  --linear-layout-item-gap: 5px;
}

.sub-layout > * {
  --linear-layout-item-gap: 20px;
}
```


**Output (Text Representation)**

```txt
The component above will lay out it's children similar to the following:

    |----------|----------|----------|----------------|
    | Anchor 0 | Anchor 1 | Anchor 2 ||--------------||
    |          |          |          || Sub Anchor 0 ||
    |          |          |          ||--------------||
    |          |          |          || Sub Anchor 1 ||
    |          |          |          ||--------------||
    |          |          |          || Sub Anchor 2 ||
    |          |          |          ||--------------||
    |----------|----------|----------|----------------|
```


# Advanced Usage: Include CSS In Import

Everything is the same as is presented in "Basic Usage" above except the imports are
slightly different:

**MyLinearLayoutComponent.jsx**

```jsx
import React from 'react';

// Import "with-css" and omit direct import of react-linear-layout.css file
import LinearLayout from 'react-linear-layout/with-css';

import './MyLinearLayoutComponent.css';


export default function MyLinearLayoutComponent(props) {
  return (
    <LinearLayout {...props} className="layout" direction="horizontal">
      <a>Anchor 0</a>
      <a>Anchor 1</a>
      <a>Anchor 2</a>
      <LinearLayout className="sub-layout" direction="vertical">
        <a>Sub Anchor 0</a>
        <a>Sub Anchor 1</a>
        <a>Sub Anchor 2</a>
      </LinearLayout>
    </LinearLayout>
  );
}
```


# API

## Props

> Any prop that is acceptable by a `div` component is acceptable by a `LinearLayout`
> component.

- **direction** `?String` - *Default = `"horizontal"`*

  The direction to arrange the component's children.

  Possible values:
    - `"horizontal"`
    - `"horiz"`
    - `"h"`
    - `"vertical"`
    - `"vert`
    - `"v"`

- **inline** `?Boolean` - *Default = `false`*

  Determines whether the layout should be treated as an inline element or a block element.

- **wrap** `?Boolean` - *Default = `false`*

  When `true`, list will be allowed to wrap instead of overflow.

- **ref** `?Function`

  [Forwarded ref][forwarding-refs] to underlying DOM element.


## CSS Custom Properties

- **--linear-layout-item-gap** `Same types as any "margin" CSS property`

  Sets the gap (I.E. margin) between all direct children of a `LinearLayout`.


## Immediate Children CSS Custom Properties

- **--linear-layout-item-gap** `Same types as any "margin" CSS property`

  Sets the gap (I.E. margin) between all direct children of a `LinearLayout`.

- **--linear-layout-item-size** `Same types as "width" or "height" CSS properties`

  Sets the height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `LinearLayout`.

- **--linear-layout-item-min-size** `Same types as "min-width" or "min-height" CSS properties`

  Sets the minimum height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `LinearLayout`.

- **--linear-layout-item-max-size** `Same types as "max-width" or "max-height" CSS properties`

  Sets the maximum height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `LinearLayout`.


<br/>
<br/>


# License

ISC License (ISC)

Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[bsara-home]: https://bsara.dev
[license]:    https://gitlab.com/bsara/react-linear-layout/blob/master/LICENSE "License"
[npm]:        https://www.npmjs.com/package/react-linear-layout                "NPM Package: react-linear-layout"

[forwarding-refs]: https://reactjs.org/docs/forwarding-refs.html "Forwarding Refs"
